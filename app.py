from flask import Flask, render_template, Response, request, redirect, url_for
from src.camera import VideoCamera
from src.Main import Main
# from CvsDetector import Test_blink
from datetime import datetime
from time import sleep
import cv2

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    cap = cv2.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        frame = camera.get_frame(frame)
        # ear = camera.getEar()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feedn')
def video_feedn():
    return Response(gen(VideoCamera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


#  Detection
# ear = 0.0

# ear = 0.0


# def genNew(camera):
#     # global ear
#     # cap = cv2.VideoCapture(0)
#     while True:
#         # ret, frame = cap.read()
#         frame = camera.detection()
#         global ear
#         ear = camera.get_ear()
#         # print(ear)
#         yield (b'--frame\r\n'
#                b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

# @app.route('/detection')
# def detection():
#     # pred = Test_blink()
#     # ear = video_detection()
#     # ear = VideoCamera.get_ear(VideoCamera())
#     # ear = url_for('get_ear')
#     return render_template('detector.html')

# @app.route('/get_ear')
# def get_ear():
#     def gen():
#         yield str(ear)
#         sleep(1)
#     return Response(gen(), mimetype='text')

# # @app.route('/time_feed')
# # def time_feed():
# #     def generate():
# #         yield datetime.now().strftime("%Y.%m.%d|%H:%M:%S")  # return also will work
# #     return Response(generate(), mimetype='text')

# @app.route('/video_detection')
# def video_detection():
#     image = genNew(VideoCamera())
#     # image = list(image)
#     # ear = get_ear(VideoCamera())
#     return Response(image,
#                     mimetype='multipart/x-mixed-replace; boundary=frame')


# @app.route('/main_test')
# def main_test():
#     return render_template('Main_app.html')

# def getFrame(camera):
#     # global ear
#     while True:
#         img = camera.main_app()
#         img = (b'--frame\r\n'
#                b'Content-Type: image/jpeg\r\n\r\n' + img + b'\r\n')
#         ear = camera.getEar()
#         total = camera.getTOTAL()
#         # print(ear)
#         # data = [img, ear]
#         yield img

# @app.route('/test')
# def test():
#     # img = Main()
#     # img = img.main_app()
#     # img = list(getFrame(Main()))
#     # img = getFrame(Main())
#     # e = gen_ear(Main())
#     return Response(getFrame(Main()),
#                     mimetype='multipart/x-mixed-replace; boundary=frame')
        

# def gen_ear(camera):
#     while True:
#         e = camera.get_ear()
#         yield e

# # @app.route('/time')
# # def time():
# #     def streamer():
# #         while True:
# #             yield f"{datetime.now()}\n"
# #             sleep(1)
# #     return Response(streamer(), mimetype='text')


# @app.route('/time_feed')
# def time_feed():
#     def generate():
#         yield datetime.now().strftime("%Y.%m.%d|%H:%M:%S")  # return also will work
#     return Response(generate(), mimetype='text')


if __name__ == "__main__":
    app.run(host='127.0.0.1', debug = True)
