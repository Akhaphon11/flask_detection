import time as t
import sys

# time_start = time.time()
# seconds = 0
# minutes = 0

def time():
    time_start = t.time()
    seconds = 0
    minutes = 0
    while True:
        try:
            sys.stdout.write("\r{minutes} Minutes {seconds} Seconds".format(minutes=minutes, seconds=seconds))
            sys.stdout.flush()
            t.sleep(1)
            seconds = int(time.time() - time_start) - minutes * 60
            if seconds >= 60:
                minutes += 1
                seconds = 0
        except KeyboardInterrupt as e:
            break

def time1():
    time_start = time.time()
    seconds = 0
    minutes = 0
    while True:
        print(f"{minutes} minutes, {seconds} seconds")
        time.sleep(1)
        seconds = int(time.time() - time_start) - minutes * 60
        if seconds >= 60:
            minutes += 1
            seconds = 0

def time2():
    for i in range(5):
        # print(i)
        sys.stdout.write(str(i))
        sys.stdout.flush()
        time.sleep(1)

time()
