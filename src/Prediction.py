import pickle
import os
from pathlib import Path

class Prediction:
    def __init__(self, ear, blink):
        self.ear  = ear
        self.blink = blink 
        self.model = self.loadModel()

    def predictor(self):
        model = self.model
        pred = model.predict([[(self.ear), (self.blink)]])
        return pred


    def loadModel(self):
        # path = 'static/file'
        # file = "C:\\Users\\AHoys\\flask_detection\\src\\CVS_Model.sav"
        # filename = "C:\\Users\\AHoys\\p-project\\new_frame\\CVS_Model.sav"
        # load_model = pickle.load(open(filename, 'rb'))
        load_model = pickle.load(
            open(os.path.join(os.getcwd(), 'flask_detection\\static','files\\CVS_Model.sav'), 'rb'))

        return load_model

# p = Prediction(0.23, 15)
# print(p.loadModel())        
        
