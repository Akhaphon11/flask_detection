import cv2
import dlib
from imutils import face_utils
from scipy.spatial import distance as dist
import numpy as np



class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')
        # self.image = image
        self.detector = self.getDetector()
        self.predictor = self.getPredictorFile()
        self.COUNTER = 0
        self.TOTAL = 0
        self.EYE_AR_THRESH = 0.23
        self.EYE_AR_CONSEC_FRAMES = 3
        self.ear = 0.0

    
    def __del__(self):
        self.video.release()
    
    def get_frame(self, pic_rgb):
        # success, image = self.video.read()
        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.
        # ret, jpeg = cv2.imencode('.jpg', image)
        ret, jpeg = cv2.imencode('.jpg', pic_rgb)
        return jpeg.tobytes()

    ############################### Test
    def detection(self):
        # image = self.video.read()

        (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
        (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

        success, image = self.video.read()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        rects = self.detector(gray,)

        for rect in rects:
            shape = self.predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)

            leftEye = shape[lStart:lEnd]
            rightEye = shape[rStart:rEnd]
            leftEAR = self.eye_aspect_ratio(leftEye)
            rightEAR = self.eye_aspect_ratio(rightEye)

            ear = (leftEAR + rightEAR) / 2.0  # float of ear
            self.ear = ear
            leftEyeHull = cv2.convexHull(leftEye)
            rightEyeHull = cv2.convexHull(rightEye)
            cv2.drawContours(image, [leftEyeHull], -1, (0, 255, 0), 1)
            cv2.drawContours(image, [rightEyeHull], -1, (0, 255, 0), 1)

            # cv2.putText(image, "Blinks: {}".format(self.TOTAL), (10, 30),
            #             cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

            cv2.putText(image, "EAR: {:.2f}".format(ear), (300, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        ret, jpeg = cv2.imencode('.jpg', image)
        jpeg = np.array(jpeg)
        jpeg = jpeg.tobytes()
        return jpeg


    #get detectorfile
    def getDetector(self):
        try:
            detector = dlib.get_frontal_face_detector()
        except:
            return "Not open detector method."
        return detector

    def getPredictorFile(self):
        try:
            predictor = dlib.shape_predictor(
                "C:\\Users\\AHoys\p-project\\video_stream_demo\\src\\shape_predictor_68_face_landmarks.dat")
        except Exception as e:
            return e
        return predictor

    def eye_aspect_ratio(self, eye):
        # compute the euclidean distances between the two sets of
        # vertical eye landmarks (x, y)-coordinates
        A = dist.euclidean(eye[1], eye[5])
        B = dist.euclidean(eye[2], eye[4])

        # compute the euclidean distance between the horizontal
        # eye landmark (x, y)-coordinates
        C = dist.euclidean(eye[0], eye[3])

        # compute the eye aspect ratio
        ear = (A + B) / (2.0 * C)

        # return the eye aspect ratio
        return ear

    def getTOTAL(self, ear):
        if ear < self.EYE_AR_THRESH:
            self.COUNTER += 1
            return 0
        else:
            if self.COUNTER >= self.EYE_AR_CONSEC_FRAMES:
                # self.TOTAL += 1
                return 1
            self.COUNTER = 0
    
    def get_ear(self):
        return self.ear

    # def get_ear(self, image):
    #     # ret, frame = self.video.read()
    #     # image = self.imgDetection(frame)
    #     (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    #     (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

    #     ear = 0.0
    #     leftEAR = 0.0
    #     rightEAR = 0.0

    #     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    #     rects = self.detector(gray,)

    #     for rect in rects:
    #         shape = self.predictor(gray, rect)
    #         shape = face_utils.shape_to_np(shape)

    #         leftEye = shape[lStart:lEnd]
    #         rightEye = shape[rStart:rEnd]
    #         leftEAR = self.eye_aspect_ratio(leftEye)
    #         rightEAR = self.eye_aspect_ratio(rightEye)

    #         ear = (leftEAR + rightEAR) / 2.0  # float of ear

    #     # return [0] ear, [1] leftEAR, [2] rightEAR
    #     return str(ear)

# if __name__ == "__main__":
#     t = VideoCamera()
#     cap = cv2.VideoCapture(0)
#     ret, frame = cap.read()
#     cap.release()
#     print(t.get_ear(frame))
