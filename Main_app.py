from flask import Flask, render_template, Response,  redirect, url_for, request, session, logging, flash
from src.Main import Main
from src.Prediction import Prediction
from time import sleep
import sys
import numpy as np
import os

import bcrypt
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from wtforms.validators import InputRequired, Length
from flask_wtf import FlaskForm
from passlib.hash import sha256_crypt
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['SECRET_KEY'] = 'Thisissupposedtobesecret!'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# urls = r'sqlite:///' + os.path.join(os.getcwd(), 'database.db')
# print(urls)
# app.config['SQLALCHEMY_DATABASE_URI'] = r'sqlite:///' + os.path.join(os.getcwd(), 'database.db')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'

Bootstrap(app)
db = SQLAlchemy(app)
# create tale in cmd "from Main_app import -> db.create_all()"

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(15))
    lastname = db.Column(db.String(50))
    username = db.Column(db.String(50))
    password = db.Column(db.String(50))


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class LoginForm(FlaskForm):
    username = StringField('username', validators=[
                           InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[
                             InputRequired(), Length(min=8, max=80)])


class RegisterForm(FlaskForm):
    firstname = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    lastname = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])

class Main_app(Main):
    def __init__(self):
        super().__init__()
        self.COUNTER = 0
        self.TOTAL = 0
        self.EYE_AR_THRESH = 0.23
        self.EYE_AR_CONSEC_FRAMES = 3
        self.frame = 0  # num of frame
        self.lsEAR = []  # list EAR of frame
        self.lsCounterBlink = []  # list value count blink
        self.avg = 0.0
        self.pred = self.predict()

    #get total of blink
    def getTOTAL(self, ear):
        if ear < self.EYE_AR_THRESH:
            self.COUNTER += 1
        else:
            if self.COUNTER >= self.EYE_AR_CONSEC_FRAMES:
                self.TOTAL += 1
            self.COUNTER = 0

    def addCounterBlink(self, blink):
        if(len(self.lsCounterBlink) == 0):
            self.lsCounterBlink.append(blink)
        else:
            self.lsCounterBlink.append(blink - sum(self.lsCounterBlink))

    def predict(self):
        # check in list is not null if null return 0
        if(len(self.lsCounterBlink) == 0):
            return 0
        else:
            # predictor assign avgEAR and blink for predictor
            # and add paramitter to prediction
            # 0 is normal
            # 1 is risk
            pred = Prediction(self.avg, self.lsCounterBlink[-1]).predictor()
            return pred

    def get_total(self):
        return self.TOTAL

    def get_avg(self):
        return self.avg

    def get_pred(self):
        return self.pred

    def get_frame(self):
        return self.frame

    def stop_frame(self):
        self.video.release()

    def gen(self):
        while True:
            image = self.main_app()
            # ear = camera.getEar()
            self.getTOTAL(self.getEar())
            self.lsEAR.append(self.getEar())
            # logic for prediction
            if (self.frame % 150) == 0.0:
                self.avg = np.average(self.lsEAR)
                print(f"num frame : {self.frame}")  # nun frame
                print(f"avg : {self.avg}")  # test ear
                print(f"total : {self.TOTAL}")  # Total blink
                # len last EAR in list
                print(f"len last lsEAR : {len(self.lsEAR)}")
                self.lsEAR.clear()
                print(f"len new lsEAR : {len(self.lsEAR)}")  # len new EAR list
                self.addCounterBlink(self.TOTAL)  # add blink to list
                print(f"list of count EAR : {self.lsCounterBlink}")
                print(f'{self.pred}')
                # Notification
                if(self.pred == 1):
                    # Notification 1
                    pass
                elif(self.pred == 2):
                    # Notification 2
                    pass
                else:
                    # Notification 3 and set 0
                    pass
            else:
                # save to csv
                pass
                    
            img = (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + image + b'\r\n')
            self.frame += 1
            yield img

# ear = 0.0
# global object
m = Main_app()

@app.route('/')
def home():
    return 'Test Main.py Application.'

# mainprogram
def gen(camera):
    while True:
        image = camera.main_app()
        ear = camera.get_avgEAR()
        
        img = (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + image + b'\r\n')
        
        yield img
        


@app.route('/main_app')
def main_app():
    return render_template('Main_app.html')

@app.route('/video_feed')
def video_feed():
    
    # frame = gen(Main())
    frame = m.gen()
    # global ear
    # ear = m.gen_avg()
    return Response(frame, mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/get_ear')
def get_ear():
    def gen():
        global m
        ear = m.get_avg()
        yield str(ear)
    return Response(gen(), mimetype='text')

@app.route('/get_total')
def get_total():
    def gen():
        global m
        total = m.get_total()
        yield str(total)
    return Response(gen(), mimetype='text')


@app.route('/get_pred')
def get_pred():
    def gen():
        global m
        pred = m.get_pred()
        yield str(pred)
    return Response(gen(), mimetype='text')


@app.route('/get_frame')
def get_frame():
    def gen():
        global m
        frame = m.get_frame()
        yield str(frame)
    return Response(gen(), mimetype='text')

@app.route("/stop" , methods=['GET', 'POST'])
def stop():
    m.stop_frame()
    return render_template('form-login.html')
    

#  Web register
@app.route('/index')
# @login_required
def index():
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                return 'loggined !!!!!!!!!!!' + current_user.username

            else:
                return "Invalid username and password"
        return '<h1>' + form.username.data + " " + form.password.data + "</h1>"

    return render_template('form-login_copy.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        new_user = User(firstname=form.firstname.data, lastname=form.lastname.data,username=form.username.data, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        return "<h1>New user has been created</h1>"
        # return '<h1>' + form.username.data + "" + form.lastname.data + " " + form.username.data + form.password.data + "</h1>"

    return render_template('form-register_copy.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

if __name__ == "__main__":
    app.run(host='127.0.0.1', debug= True)


# scrpt export csv file = 'sqlite3 -header -csv c:/sqlite/chinook.db "select * from tracks;" > tracks.csv'
