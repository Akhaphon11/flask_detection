from src.camera import VideoCamera
import cv2
import dlib
from imutils import face_utils
from scipy.spatial import distance as dist
import numpy as np
import os
from flask import url_for, redirect


class Main(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        self.detector = self.getDetector()
        self.predictor = self.getPredictorFile()
        self.ear = 0.0

    def __del__(self):
        self.video.release()

    def stop(self):
        self.video.release()

    def main_app(self):
        # # while True:
        # #     ret, frame = self.video.read()
        # #     image = self.imgDetection(frame)
        # #     self.ear = self.get_ear(frame)[0]
        # #     return [image, self.ear]
        ret, frame = self.video.read()
        image = self.imgDetection(frame)
        return image
        
            



    def imgDetection(self, frame):
        (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
        (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
        # ret, frame = self.video.read()
        # success, image = self.video.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        rects = self.detector(gray,)

        for rect in rects:
            shape = self.predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)

            leftEye = shape[lStart:lEnd]
            rightEye = shape[rStart:rEnd]
            leftEAR = self.eye_aspect_ratio(leftEye)
            rightEAR = self.eye_aspect_ratio(rightEye)

            self.ear = (leftEAR + rightEAR) / 2.0  # float of ear
            # self.ear = ear
            leftEyeHull = cv2.convexHull(leftEye)
            rightEyeHull = cv2.convexHull(rightEye)
            cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
            cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

            # cv2.putText(image, "Blinks: {}".format(self.TOTAL), (10, 30),
            #             cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

            cv2.putText(frame, "EAR: {:.2f}".format(self.ear), (300, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        ret, jpeg = cv2.imencode('.jpg', frame)
        jpeg = np.array(jpeg)
        jpeg = jpeg.tobytes()
        return jpeg
            
    #get detectorfile
    def getDetector(self):
        try:
            detector = dlib.get_frontal_face_detector()
        except Exception as e:
            return e
        return detector

    def getPredictorFile(self):
        try:
            path = os.getcwd()
            # path = os.path.abspath(os.path.join(path, os.pardir))
            # pred = os.path.join(os.getcwd(), 'flask_detection','static', 'files','shape_predictor_68_face_landmarks.dat')
            # pred = "C:\\Users\\AHoys\\flask_detection\\static\\files\\shape_predictor_68_face_landmarks.dat"
            predictor = dlib.shape_predictor('static/files/shape_predictor_68_face_landmarks.dat')
            # predictor = dlib.shape_predictor(pred)
        except Exception as e:
            return e
        return predictor

    def eye_aspect_ratio(self, eye):
        # compute the euclidean distances between the two sets of
        # vertical eye landmarks (x, y)-coordinates
        A = dist.euclidean(eye[1], eye[5])
        B = dist.euclidean(eye[2], eye[4])

        # compute the euclidean distance between the horizontal
        # eye landmark (x, y)-coordinates
        C = dist.euclidean(eye[0], eye[3])

        # compute the eye aspect ratio
        ear = (A + B) / (2.0 * C)

        # return the eye aspect ratio
        return ear

    def get_ear(self):
        ret, frame = self.video.read()
        image = self.imgDetection(frame)
        (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
        (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

        ear = 0.0
        leftEAR = 0.0
        rightEAR = 0.0

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        rects = self.detector(gray,)

        for rect in rects:
            shape = self.predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)

            leftEye = shape[lStart:lEnd]
            rightEye = shape[rStart:rEnd]
            leftEAR = self.eye_aspect_ratio(leftEye)
            rightEAR = self.eye_aspect_ratio(rightEye)

            ear = (leftEAR + rightEAR) / 2.0  # float of ear

        # return [0] ear, [1] leftEAR, [2] rightEAR
        return [ear, leftEAR, rightEAR]

    def getEar(self):
        return self.ear

    def getTOTAL(self):
        return self.TOTAL

